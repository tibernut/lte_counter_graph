
#Standard Library imports
import tkinter as tk
from tkinter import ttk
import glob
import datetime
import os
#Import custom scripts
import fileListBuilder
import LTE_stat_XMLparse
import LTE_stat_Plot
import file_manip
from radioreport import getMimo


class MainWindow:
    def __init__(self, master):
        '''
        Build and display the GUI
        '''

        #Get temporary files path and ltestats path
        with open('path.txt', 'r') as pathread:
            self.pathlist = pathread.readlines()
            self.path = self.pathlist[0][:-1]
            self.wpath = self.pathlist[1]
            self.wpath = self.wpath.rstrip()

        #Load available counters
        with open('counters.txt', 'r') as f:
            self.counterList = [line.strip() for line in f]

        #Build eNodeB List for display in GUI
        self.siteList = fileListBuilder.getSiteList()

        #Get available dates and hours for display in GUI
        self.dateList = []
        self.dateObjList = fileListBuilder.availableDates()
        for d in self.dateObjList:
            self.dateList.append(str(d))
        self.starthourList=list(range(24))
        self.endhourList=list(range(24))

        #define GUI variables
        self.master = master
        self.frame = tk.Frame(self.master)
        self.master.title("LTE RAN Stats")
        self.sitevar = tk.StringVar()
        self.sitevar.set(self.siteList[0])
        self.startDateVar = tk.StringVar()
        self.startDateVar.set(self.dateList[-1])
        self.endDateVar = tk.StringVar()
        self.endDateVar.set(self.dateList[-1])
        self.startHourVar = tk.StringVar()
        self.startHourVar.set(self.starthourList[0])
        self.lastHourVar = tk.StringVar()
        self.lastHourVar.set(self.endhourList[-1])

        #Build Labels
        self.sdateLBL = tk.Label(self.frame, justify='left', text='Start Date')
        self.edateLBL = tk.Label(self.frame, justify='left', text='End Date')
        self.shourLBL = tk.Label(self.frame, justify='left', text='Start Hour')
        self.ehourLBL = tk.Label(self.frame, justify='left', text='End Hour')
        self.enbLBL = tk.Label(self.frame, justify='left', text='Target Site')
        self.searchlbl = tk.Label(self.frame, justify='left', text='Type to Search')
        self.counterLBL = tk.Label(self.frame, justify='left', text='Counter/KPI')

        #Build GUI Elements
        self.search_var = tk.StringVar()
        self.search_var.trace("w", lambda name, index, mode: self.update_list())
        self.entry = tk.Entry(self.frame, textvariable=self.search_var, width=13)
        self.eNodeBs = ttk.Combobox(self.frame, values=self.siteList, state='readonly', textvariable= self.sitevar, width= 50)
        self.startDate = ttk.Combobox(self.frame, values=self.dateList, state='readonly', textvariable= self.startDateVar, width= 10)
        self.endDate = ttk.Combobox(self.frame, values=self.dateList, state='readonly', textvariable= self.endDateVar, width= 10)
        self.lastHour = ttk.Combobox(self.frame, values=self.endhourList, state='readonly', textvariable= self.lastHourVar, width= 5)
        self.startHour = ttk.Combobox(self.frame, values=self.starthourList, state='readonly', textvariable= self.startHourVar, width= 5)
        self.Accept = tk.Button(self.frame, text="Accept", command=lambda: self.runReport())
        self.yScroll = tk.Scrollbar(self.frame, orient=tk.VERTICAL)
        self.listbox = tk.Listbox(self.frame,
             yscrollcommand=self.yScroll.set)
        self.yScroll['command'] = self.listbox.yview
        self.update_list()

        #Place objects in Window
        self.yScroll.grid(row=5, column=5, sticky='ns')
        self.searchlbl.grid(row=4, column=1)
        self.listbox.grid(row=5, column=2, columnspan=3, sticky='nsew')
        self.entry.grid(row=4, column=2, columnspan=3, sticky='ew')
        self.sdateLBL.grid(row=1, column=1)
        self.edateLBL.grid(row=2, column=1)
        self.shourLBL.grid(row=1, column=3, sticky='e')
        self.ehourLBL.grid(row=2, column=3, sticky='e')
        self.enbLBL.grid(row=3, column=1)
        self.counterLBL.grid(row=5, column=1, sticky='n')
        self.startDate.grid(row=1, column=2, sticky='sw')
        self.startHour.grid(row=1, column=4, sticky='sw')
        self.endDate.grid(row=2, column=2, sticky='sw')
        self.lastHour.grid(row=2, column=4, sticky='sw')
        self.eNodeBs.grid(row=3, column=2, columnspan=3, sticky='se')
        self.Accept.grid(row=6, column=2, columnspan=3, sticky='nesw')
        self.frame.grid()


    def update_list(self):
        '''
        Updates the counters present in self.listbox by matching charactors
        in self.entry
        '''

        search_term = self.search_var.get()
        self.listbox.delete(0, tk.END)
        for item in self.counterList:
                if search_term.lower() in item.lower():
                    self.listbox.insert(tk.END, item)


    def runReport(self):
        '''
        Activated when pressing 'Accept' in the GUI.  This reads the values from
        the GUI elements and passes them to other scripts for processing and
        generating the graph.
        '''

        #Get user values from GUI
        self.enodeb = self.sitevar.get()
        self.startd = self.startDateVar.get()
        self.endDate = self.endDateVar.get()
        self.starth = datetime.time(int(self.startHourVar.get()))
        self.lasth =  datetime.time(int(self.lastHourVar.get()))
        self.scounter = self.listbox.get(tk.ACTIVE)

        #Build list of files to collect stats from
        self.startdatetime = datetime.datetime.combine(datetime.datetime.strptime(self.startd, '%Y-%m-%d'), self.starth)
        self.enddatetime = datetime.datetime.combine(datetime.datetime.strptime(self.endDate, '%Y-%m-%d'), self.lasth)
        #sanity check
        if self.startdatetime < self.enddatetime:
            if self.scounter =="KPI.MimoEligibility":
                getMimo(self.enodeb, self.startdatetime, self.enddatetime)
            else:
                self.preList = fileListBuilder.identifyFiles(self.enodeb, self.startdatetime, self.enddatetime)
                #send fileslist to cleanup, this will delete all files in directory that aren't needed
                #It ensures only the contents of your last query are in the working directory
                file_manip.cleanupData(self.preList)
                #Send list to unzipList which will unzip all files in list not already in working directory
                file_manip.unzipList(self.preList)
                #Create list of files in working directory and send that list to the XML parse function to decode
                #the XML and pull the required counters.  Function also returns list of moids which we use
                #For the graph's legend
                self.fileList = []
                for file in glob.glob(os.path.join(self.wpath, '*')):
                    self.fileList.append(file)
                self.moidList = LTE_stat_XMLparse.parseStat(self.fileList, self.scounter)
                #finally start the graphing function
                LTE_stat_Plot.drawGraph(self.moidList, self.scounter)
        else:
            print("start datetime must be less than end datetime")


def main():
    root = tk.Tk()
    app = MainWindow(root)
    root.mainloop()


if __name__ == '__main__':
    main()
