### 9926 Counters Graph

You can graph counters collected on your 9926 Nokia (formerly Alcate-Lucent) eNodeBs using this script.

You must provide the script access to the xml documents that contain the counters.  Typically these can be found on your 5620 SAM server in the /opt/5620sam/lte/stats/ directory.  I would suggest downloading a copy of these files to your local workstation.  After that you must specify the path to the files in path.txt.

!!!
Make sure you edit path.txt
!!!
On the first line you need to point to the directory on you computer that will hold the files from SAM
AND
On the second line you need to point to the directory on your computer that will hold temporary files.
It is best to create a brand new empty folder for this purpse - everything in the folder will be deleted in between creating graphs!!!

The script simply allows you to graph the raw counter values over time.  I have defined 1 KPI, MIMO eligibility rate, and intend on defining more in the future.

The interface should be fairly self-explanatory.  

![Interface Example](http://sanitizedinput.com/static/images/LTE_counter_graph_image.png)
